《微信中控服务接口定义》



[接口文档码云地址](https://gitee.com/yian_outdoor/doc)

# 概述

微信中控服务器（WXCC）提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}`或`https://{host}`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 获取 jssdk 授权

**URL**

/wxcc/jssdk?url=`url`

**HTTP 请求方式**

GET

**请求体**

| 参数 | 必填 | 类型   | 说明                                                  |
| ---- | ---- | ------ | ----------------------------------------------------- |
| url  | 是   | string | 当前位置的 location.href,必须`encodeURIComponent`转码 |

**响应体**

| 参数      | 必填 | 类型   | 说明         |
| --------- | ---- | ------ | ------------ |
| appid     | 是   | string | 公众号 appid |
| timestamp | 是   | string | 时间戳       |
| noncestr  | 是   | string | 随机字符串   |
| signature | 是   | string | 签名         |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           appid: '11212121212',
           timestamp: '121212122',
           noncestr: '2121211',
           signature: '2121212121'
        }
    }
```

## 微信授权二连跳

**URL**

/wxcc/redirect?state=`state`&data=`data`

**HTTP 请求方式**

GET

**请求体**

| 参数  | 必填 | 类型   | 说明                                                               |
| ----- | ---- | ------ | ------------------------------------------------------------------ |
| state | 是   | string | 前后端约定好的跳转状态值，由此参数决定，微信回调之后服务器跳转地址 |
| data  | 是   | string | 服务端回跳之后的 url 中传递下去的参数                              |

**响应体**

| 参数 | 必填 | 类型   | 说明                                        |
| ---- | ---- | ------ | ------------------------------------------- |
| code | 是   | string | 微信用户 code，可以根据此 code 获取用户信息 |
| data | 是   | string | 由第一次跳转传递下来的数据                  |

```
// url 示例

https://www.yianhuwai.club/${path}?code=`state`&data=`data`
```

## 根据 code 得到用户信息

**URL**

/wxcc/info?code=`code`

**HTTP 请求方式**

GET

**请求体**

| 参数 | 必填 | 类型   | 说明          |
| ---- | ---- | ------ | ------------- |
| code | 是   | string | 微信用户 code |

**响应体**

| 参数      | 必填 | 类型   | 说明                                                          |
| --------- | ---- | ------ | ------------------------------------------------------------- |
| userid    | 是   | int    | 用户 6 位数 userid                                            |
| openid    | 是   | string | 微信用户在公众号的 openid                                     |
| role      | 是   | int    | 用户角色 1 鱼塘塘主 0 普通用户                                |
| nickname  | 是   | string | 用户昵称                                                      |
| gender    | 是   | int    | 用户的性别, 值为 1 时是男性, 值为 2 时是女性, 值为 0 时是未知 |
| city      | 是   | string | 普通用户个人资料填写的城市                                    |
| province  | 是   | string | 用户个人资料填写的省份                                        |
| country   | 是   | string | 国家                                                          |
| avatarurl | 是   | string | 用户头像 URL，用户没有头像时该项为空。                        |
| mobile    | 是   | string | 用户手机号，第一次调用为空                                    |
| address   | 是   | string | 用户详细地址                                                  |
| subscribe | 是   | int    | 用户是否关注公众号                                            |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           userid: 100001,
           openid: 'dfdfdfdfdfdfdf',
           nickname: 'xiyangyang',
           gender: 1,
           mobile: '1121212121',
           address: '滨江区',
           city: '杭州',
           province: '浙江',
           country: '中国',
           avatarurl: 'https://avatar.com/1.png',
           subscribe: 1
        }
    }
```

## 微信支付接口

**URL**

/wxcc/pay

**HTTP 请求方式**

POST

**请求体**

| 参数   | 必填 | 类型   | 说明               |
| ------ | ---- | ------ | ------------------ |
| userid | 是   | int    | 用户六位数 id      |
| openid | 是   | string | 用户 openid        |
| amount | 是   | int    | 支付金额，单位 分  |
| remark | 是   | string | 备注，支付备注信息 |

**响应体**

| 参数      | 必填 | 类型   | 说明                     |
| --------- | ---- | ------ | ------------------------ |
| noncestr  | 是   | string | 微信支付需要的随机字符串 |
| timestamp | 是   | string | 微信支付需要的时间戳     |
| package   | 是   | string | 微信支付的统一下单 id    |
| signType  | 是   | string | 微信支付的签名方式       |
| paySign   | 是   | string | 支付签名                 |
| payid     | 是   | string | 支付订单号               |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           noncestr: '11212121212',
           timestamp: '121212122',
           package: '2121211',
           signType: '2121212121'，
           paySign： '121212112',
           payid: 'payment12121212121'
        }
    }
```

## 微信获取订单接口

**URL**

/wxcc/order

**HTTP 请求方式**

GET

**请求体**

| 参数  | 必填 | 类型 | 说明                    |
| ----- | ---- | ---- | ----------------------- |
| payid | 是   | int  | 订单 id，由支付接口返回 |

**响应体**

| 参数      | 必填 | 类型   | 说明                     |
| --------- | ---- | ------ | ------------------------ |
| userid  | 是   | int | 用户id |
| openid | 是   | string | 微信openid     |
| state   | 是   | int | 支付成功与否 1 成功 0 失败  |
| out_trade_no  | 是   | string | 支付订单号       |
| remark   | 是   | string | 支付备注                 |


```
    {
        code: 1,
        msg: 'ok',
        data: {
           userid: 10000,
           opendi: '121212122',
           state: 1,
           out_trade_no: '2121212121'，
           remark '121212112'
        }
    }
```



## 微信发送模板消息

**URL**

/wxcc/message

**HTTP 请求方式**

POST

**请求体**

| 参数  | 必填 | 类型 | 说明                    |
| ----- | ---- | ---- | ----------------------- |
| templateid | 是   | int  | 模板id |
| userid | 是   | int | 用户id     |
| url   | 是   | string | 消息要跳转的路径 |
| data  | 是   | Object | 对应模板id的对应内容       |
**响应体**

无

```
    {
        code: 1,
        msg: 'ok',
        data: {}
    }
```
