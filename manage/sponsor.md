《绎安户外 pc 服务端赞助商接口定义》

# 概述

绎安户外 pc 管理服务端(manage)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/manage`或`https://{host}/manage`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/manage/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 赞助商列表获取

**URL**

/sponsor/list

**HTTP 请求方式**

POST

**请求体**

> 赞助商列表查询接口，如果 cid、tid、name 均不传，则查找所有赞助商

| 参数     | 必填 | 类型   | 说明                                      |
| -------- | ---- | ------ | ----------------------------------------- |
| adstate  | 否   | int    | 赞助商广告状态 0 未生效 1 已启用 2 已过期 |
| sname    | 否   | string | 赞助商名称                                |
| pagenum  | 是   | int    | 页码,从 1 开始                            |
| pagesize | 是   | int    | 页大小                                    |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 赞助商列表                           |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数      | 必填 | 类型          | 说明                                      |
| --------- | ---- | ------------- | ----------------------------------------- |
| sid       | 是   | int           | 赞助商 id                                 |
| sname     | 是   | string        | 赞助商名称                                |
| mobile    | 是   | string        | 联系人电话                                |
| address   | 是   | string        | 详细地址                                  |
| contact   | 是   | string        | 联系人                                    |
| logo      | 是   | string        | 赞助商 logo 图片地址                      |
| adstate   | 是   | int           | 赞助商广告状态 0 未生效 1 已启用 2 已过期 |
| adcontent | 是   | string        | 广告内容                                  |
| fistlist  | 是   | array<object> | 赞助商赞助的鱼塘列表                      |

fistlist 数组中的对象 object 格式

| 参数      | 必填 | 类型   | 说明                       |
| --------- | ---- | ------ | -------------------------- |
| fid       | 是   | int    | 鱼塘 fid                   |
| fname     | 是   | string | 鱼塘名称                   |
| tid       | 是   | int    | 鱼塘类型 id                |
| tname     | 是   | string | 鱼塘类型名称               |
| cid       | 是   | int    | 所属城市 id                |
| cname     | 是   | string | 所属城市名称               |
| address   | 是   | string | 详细地址                   |
| contact   | 是   | string | 联系人                     |
| mobile    | 是   | string | 联系人电话                 |
| vid       | 是   | int    | 核销人 id                  |
| vname     | 是   | string | 核销人名称                 |
| salesman  | 是   | string | 业务员名称                 |
| starttime | 是   | int    | 开始赞助时间，格式为时间戳 |
| endtime   | 是   | int    | 终止赞助时间，时间为时间戳 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalpage: 3,
            totalcount: 10,
            list: [
               {
                    sid: 1,
                    sname: '钱塘江',
                    address: '浙江杭州钱塘江大桥',
                    contact: '马云',
                    mobile: '15555555555',
                    adstate: 0,
                    adcontent: '我是广告内容'
                    fishlist: [
                      {
                        fid: 1,
                        fname: '钱塘江大桥鱼塘',
                        tid: 1，
                        tname: '黑坑',
                        cid: 1，
                        cname: '浙江杭州',
                        address: '哈哈哈哈哈哈',
                        contact: '马化腾',
                        mobile: '1555555555',
                        vid: 1,
                        vname: '李彦宏',
                        salesman: '马云',
                        starttime: 12323223232323,
                        endtime: 1211212121212
                      }
                    ]
               }
           ]
        }
    }
```

## 新建赞助商

**URL**

/sponsor/new

**HTTP 请求方式**

POST

**请求体**

| 参数    | 必填 | 类型   | 说明       |
| ------- | ---- | ------ | ---------- |
| sname   | 是   | string | 赞助商名称 |
| address | 是   | string | 详细地址   |
| contact | 是   | string | 联系人     |
| mobile  | 是   | string | 联系人电话 |
| logo    | 是   | string | logo 地址  |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `新建成功` 0 `新建失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 赞助商删除

**URL**

/sponsor/delete

**HTTP 请求方式**

POST

**请求体**

| 参数 | 必填 | 类型 | 说明      |
| ---- | ---- | ---- | --------- |
| sid  | 是   | int  | 赞助商 id |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `删除成功` 0 `删除失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 赞助商编辑

**URL**

/sponsor/editor

**HTTP 请求方式**

POST

**请求体**

| 参数      | 必填 | 类型   | 说明       |
| --------- | ---- | ------ | ---------- |
| sid       | 是   | int    | 赞助商 id  |
| sname     | 是   | string | 赞助商名称 |
| contact   | 是   | string | 联系人     |
| mobile    | 是   | string | 联系人电话 |
| address   | 是   | string | 详细地址   |
| logo      | 是   | string | logo 地址  |
| adcontent | 是   | string | 广告内容   |
| fishlist  | 是   | array  | 赞助鱼塘   |

fishlist 数组

| 参数      | 必填 | 类型 | 说明           |
| --------- | ---- | ---- | -------------- |
| fid       | 是   | int  | 鱼塘 id        |
| starttime | 是   | int  | 开始时间时间戳 |
| endtime   | 是   | int  | 终止时间时间戳 |

```
  fishlist: [
    {
      fid: 1,
      startime: 121212121212,
      endtime: 121211212121,
    }
  ]
```

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `编辑成功` 0 `编辑失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 获取绎安户外的信息

**URL**

/sponsor/master

**HTTP 请求方式**

GET

**请求体**

无

**响应体**

| 参数 | 必填 | 类型 | 说明 |
| ------- | ---- | ------ | ---------- |
| sname | 是 | string | 赞助商名称 |
| address | 是 | string | 详细地址 |
| contact | 是 | string | 联系人 |
| mobile | 是 | string | 联系人电话 |
| logo | 是 | string | logo 地址 |
| adcontent | 是 | string | 广告内容 |


```
    {
        code: 1,
        msg: 'ok',
        data: {
           name: '议案户外',
           address: '北京市',
           contact: '张三'，
           mobile: '1222222222',
           logo: 'https://logo.png',
           adcontent: '这里是广告字符'
        }
    }
```

## 修改绎安户外的信息

**URL**

/sponsor/master

**HTTP 请求方式**

POST

**请求体**

| 参数 | 必填 | 类型 | 说明 |
| ------- | ---- | ------ | ---------- |
| sname | 是 | string | 赞助商名称 |
| address | 是 | string | 详细地址 |
| contact | 是 | string | 联系人 |
| mobile | 是 | string | 联系人电话 |
| logo | 是 | string | logo 地址 |
| adcontent | 是 | string | 广告内容 |

**响应体**

| 参数 | 必填 | 类型 | 说明 |
| ----- | ---- | ---- | ------------------------- |
| state | 是 | int | 1 `编辑成功` 0 `编辑失败` |
| desc | 是 | int | 1 `success` 0 `fail` |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```
