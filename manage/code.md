《绎安户外 pc 服务端二维码接口定义》

# 概述

绎安户外 pc 管理服务端(manage)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/manage`或`https://{host}/manage`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/manage/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 鱼塘二维码查询

**URL**

/code/fishlist

**HTTP 请求方式**

POST

**请求体**

> 二维码列表查询接口，如果 name 不传，则查找所有鱼塘下的二维码

| 参数     | 必填 | 类型   | 说明           |
| -------- | ---- | ------ | -------------- |
| fname    | 否   | string | 鱼塘名称       |
| pagenum  | 是   | int    | 页码,从 1 开始 |
| pagesize | 是   | int    | 页大小         |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 二维码列表                           |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数     | 必填 | 类型          | 说明               |
| -------- | ---- | ------------- | ------------------ |
| fid      | 否   | int           | 鱼塘 fid           |
| codeid   | 否   | int           | 二维码 id          |
| fname    | 是   | string        | 鱼塘名称           |
| codelist | 是   | array<object> | 鱼塘下的二维码列表 |

codelist 数组中的对象 object 格式

| 参数           | 必填 | 类型   | 说明                                                 |
| -------------- | ---- | ------ | ---------------------------------------------------- |
| codestate      | 是   | int    | 二维码状态 0 未激活 1 未领取 2 已领取                |
| codeid         | 是   | int    | 二维码 id                                            |
| session        | 是   | int    | 场次                                                 |
| codestarttime  | 是   | int    | 二维码生效时间，格式为时间戳                         |
| codeendtime    | 是   | int    | 二位码实效时间，格式为时间戳                         |
| specialprize   | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| pid            | 是   | int    | 奖品 pid                                             |
| prizetype      | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount         | 是   | int    | 平台红包，单位是分                                   |
| others         | 是   | string | 其他优惠                                             |
| freestate      | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount     | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| couponstartime | 是   | int    | 优惠卷开始时间，格式为时间戳                         |
| couponendtime  | 是   | int    | 优惠卷失效时间，格式为时间戳                         |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalpage: 3,
            totalcount: 10,
            list: [
               {
                    fid: 1,
                    fname: '钱塘江',
                    codelist: [
                      {
                        codeid: 0001，
                        codestate: 1,
                        session: 1,
                        codestarttime: 121212121212,
                        codeendtime: 1212121212,
                        specialprize： 0,
                        pid: 1,
                        prizetype: 1,
                        amount: 12121,
                        others: '哈哈哈哈',
                        freestate: 1,
                        freeamount: 12121,
                        couponstartime: 22121212212,
                        couponendtime: 1212121323223
                      }
                    ]
               }
           ]
        }
    }
```

## 二维码编号, 场次， 有效期，二维码状态 查询接口

**URL**

/code/find

**HTTP 请求方式**

POST

**请求体**

> 二维码列表查询接口，如果 codeid,codestate,session, starttime,endtime 都不传，则返回空数组

| 参数      | 必填 | 类型 | 说明                                                         |
| --------- | ---- | ---- | ------------------------------------------------------------ |
| codeid    | 否   | int  | 二维码 id                                                    |
| fid       | 否   | int  | 鱼塘 id                                                      |
| codestate | 否   | int  | 二维码状态 0 未激活 1 未领取 2 已领取                        |
| session   | 否   | int  | 场次                                                         |
| starttime | 否   | int  | 二维码生效时间，格式为时间戳                                 |
| endtime   | 否   | int  | 二维码失效时间，格式为时间戳， starttime 和 endtime 成对出现 |
| pagenum   | 是   | int  | 页码,从 1 开始                                               |
| pagesize  | 是   | int  | 页大小                                                       |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 二维码列表                           |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

list 中 object 格式

| 参数            | 必填 | 类型   | 说明                                                 |
| --------------- | ---- | ------ | ---------------------------------------------------- |
| codestate       | 是   | int    | 二维码状态 0 未激活 1 未领取 2 已领取                |
| codeid          | 是   | int    | 二维码 id                                            |
| session         | 是   | int    | 场次                                                 |
| codestarttime   | 是   | int    | 二维码生效时间，格式为时间戳                         |
| codeendtime     | 是   | int    | 二位码实效时间，格式为时间戳                         |
| specialprize    | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| prizetype       | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount          | 是   | int    | 平台红包，单位是分                                   |
| others          | 是   | string | 其他优惠                                             |
| pid             | 否   | int    | 奖品 id                                              |
| freestate       | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount      | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| couponstarttime | 是   | int    | 优惠卷开始时间，格式为时间戳                         |
| couponendtime   | 是   | int    | 优惠卷失效时间，格式为时间戳                         |
| userid   | 是   | int    | 领取人id                    |
| nickname   | 是   | string    | 领取人昵称                   |
| gettime   | 是   | int    | 领取时间                   |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            list: [
              codeid: 0001，
              codestate: 1,
              session: 1,
              codestarttime: 121212121212,
              codeendtime: 1212121212,
              specialprize： 0,
              pid: 1,
              prizetype: 1,
              amount: 12121,
              others: '哈哈哈哈',
              freestate: 1,
              freeamount: 12121,
              couponstartime: 22121212212,
              couponendtime: 1212121323223,
              userid:11111,
              nickname: 'hshfhdfdf',
              gettime: 423421212
            ]
        }
    }
```



## 二维码编辑

**URL**

/code/editor

**HTTP 请求方式**

POST

**请求体**

| 参数  | 必填 | 类型 | 说明           |
| ----- | ---- | ---- | -------------- |
| codeid | 是   | int  | 二维码id |  |
| pid | 是   | int  | 奖品id |  |
| cendtime | 是   | int  | 优惠券失效时间 |  |
| codestarttime | 是   | int  | 二维码开始日期 |  |
| codeendtime | 是   | int  | 二维码失效日期 |  |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `修改成功` 0 `修改失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 二维码生成

**URL**

/code/new

**HTTP 请求方式**

POST

**请求体**

| 参数  | 必填 | 类型 | 说明           |
| ----- | ---- | ---- | -------------- |
| count | 是   | int  | 新增二维码数量 |  |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `新建成功` 0 `新建失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 二维码下载

**URL**

/code/load

**HTTP 请求方式**

POST

**请求体**

| 参数       | 必填 | 类型  | 说明               |
| ---------- | ---- | ----- | ------------------ |
| codeidlist | 是   | array | 下载二维码 id 列表 |

**响应体**

| 参数        | 必填 | 类型 | 说明           |
| ----------- | ---- | ---- | -------------- |
| url | 是   | int  | 压缩包地址 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
          url: 'https://...'
        }
    }
```

## 二维码绑定

**URL**

/code/bind

**HTTP 请求方式**

POST

**请求体**

| 参数  | 必填 | 类型 | 说明           |
| ----- | ---- | ---- | -------------- |
| count | 是   | int  | 绑定二维码数量 |
| fid   | 是   | int  | 绑定鱼塘 id    |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `绑定成功` 0 `绑定失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 二维码激活

**URL**

/code/activate

**HTTP 请求方式**

POST

**请求体**

> 此处，二维码数量必须等于或者大于奖品数量

| 参数          | 必填 | 类型  | 说明                             |
| ------------- | ---- | ----- | -------------------------------- |
| codeidlist    | 是   | array | 二维码 id 数组                   |
| pidlist       | 是   | array | 奖品 id 数组                     |
| cendtime      | 是   | int   | 优惠卷失效时间，格式为时间戳     |
| codestarttime | 是   | int   | 二维码有效开始时间，格式为时间戳 |
| codeendtime   | 是   | int   | 二维码失效时间，格式为时间戳     |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `激活成功` 0 `激活失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 二维码最大编号查询

**URL**

/code/max

**HTTP 请求方式**

GET

**请求体**

无
**响应体**

| 参数 | 必填 | 类型 | 说明     |
| ---- | ---- | ---- | -------- |
| max  | 是   | int  | 最大编号 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           max: 1,
        }
    }
```

## 二维码数量查询

**URL**

/code/count

**HTTP 请求方式**

GET

**请求体**

> 如果没有传 codestate ,默认取未激活的

| 参数      | 必填 | 类型 | 说明                                  |
| --------- | ---- | ---- | ------------------------------------- |
| codestate | 是   | int  | 二维码状态 0 未激活 1 未领取 2 已领取 |

**响应体**

| 参数  | 必填 | 类型 | 说明       |
| ----- | ---- | ---- | ---------- |
| count | 是   | int  | 二维码数量 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           count: 1,
        }
    }
```
