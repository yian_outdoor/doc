《绎安户外 pc 服务端日志接口定义》

# 概述

绎安户外 pc 管理服务端(manage)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/manage`或`https://{host}/manage`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/manage/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 城市列表获取

**URL**

/logger/list

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型 | 说明           |
| -------- | ---- | ---- | -------------- |
| pagenum  | 是   | int  | 页码,从 1 开始 |
| pagesize | 是   | int  | 页大小         |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 城市列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数      | 必填 | 类型   | 说明                        |
| --------- | ---- | ------ | --------------------------- |
| operateid | 是   | int    | 操作模块的 ID               |
| module    | 是   | string | 模块名称                    |
| account   | 是   | string | 账户                        |
| role      | 是   | int    | 1 管理员 0 业务员           |
| message   | 是   | string | 具体信息                    |
| handle    | 是   | string | 1 删除 2 编辑 3 添加 4 激活 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           list: [
               {
                    operateid: 1,
                    module: '用户',
                    account: 'land',
                    role: 1,
                    message: '删除用户',
                    handle: 1
               },
               {
                    operateid: 1,
                    module: '用户',
                    account: 'land',
                    role: 1,
                    message: '删除用户',
                    handle: 1
               }
           ]
        }
    }
```
