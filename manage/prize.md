《绎安户外 pc 服务端奖品接口定义》

# 概述

绎安户外 pc 管理服务端(manage)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/manage`或`https://{host}/manage`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/manage/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 奖品列表获取

**URL**

/prize/list

**HTTP 请求方式**

POST

**请求体**

> 奖品列表查询接口

| 参数         | 必填 | 类型 | 说明                     |
| ------------ | ---- | ---- | ------------------------ |
| specialprize | 是   | int  | 是否为特等奖 1 是 0 不是 |
| pagenum      | 是   | int  | 页码,从 1 开始           |
| pagesize     | 是   | int  | 页大小                   |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 奖品列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| pid          | 是   | int    | 奖品 pid                                             |
| pname        | 是   | string | 奖品名称                                             |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| number       | 是   | int    | 特等奖位次, 只有为特等奖时有效                       |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalpage: 3,
            totalcount: 10,
            list: [
               {
                  pid: 1,
                  pname: '钱塘江',
                  prizetype: 1,
                  number: 1,
                  amount: 12,
                  others: '鱼钩',
                  freestate: 1,
                  freeamount: 0
               }
           ]
        }
    }
```

## 新建奖品

**URL**

/prize/new

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型   | 说明                                                                      |
| ------------ | ---- | ------ | ------------------------------------------------------------------------- |
| pname        | 是   | string | 奖品名称                                                                  |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                                                  |
| number       | 是   | int    | 特等奖的位次，只有为特等奖时有效                                          |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖                                   |
| amount       | 是   | int    | 奖品金额,单位分                                                           |
| others       | 是   | string | 实物奖备注                                                                |
| freestate    | 否   | int    | 是否免费 1 免费 0 不免费, 此参数只有普通奖需要                            |
| freeamount   | 否   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 此参数只有普通奖需要 |
| fishidlist   | 否   | list   | 鱼塘 fid 列表，此参数只有为特等奖时需要                                   |
| starttime    | 否   | int    | 生效时间, 此参数只有特等奖需要                                            |
| endtime      | 否   | int    | 失效时间, 此参数只有特等奖需要                                            |
| vid          | 否   | int    | 核销人 id, 此参数只有特等奖需要                                           |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `新建成功` 0 `新建失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 奖品删除

**URL**

/prize/delete

**HTTP 请求方式**

POST

**请求体**

| 参数 | 必填 | 类型 | 说明     |
| ---- | ---- | ---- | -------- |
| pid  | 是   | int  | 奖品 pid |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `删除成功` 0 `删除失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 奖品编辑

**URL**

/prize/editor

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| pid          | 是   | int    | 奖品 pid                                             |
| pname        | 是   | string | 奖品名称                                             |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| fishidlist   | 否   | list   | 鱼塘 fid 列表，此参数只有为特等奖时需要              |
| number       | 是   | int    | 特等奖出现的位次，只有为特等奖时有效                 |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖 3            |
| amount       | 是   | int    | 奖品金额,单位分                                      |
| others       | 是   | string | 实物奖备注                                           |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `编辑成功` 0 `编辑失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 特等奖品记录列表获取

**URL**

/prize/records

**HTTP 请求方式**

POST

**请求体**

> 奖品列表查询接口

| 参数     | 必填 | 类型 | 说明                    |
| -------- | ---- | ---- | ----------------------- |
| success  | 是   | int  | 1 为已经领取 0 为未领取 |
| pagenum  | 是   | int  | 页码,从 1 开始          |
| pagesize | 是   | int  | 页大小                  |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 奖品列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| rid          | 是   | int    | 奖品记录 id                                          |
| pid          | 是   | int    | 奖品 id                                              |
| pname        | 是   | string | 奖品名称                                             |
| fid          | 是   | int    | 鱼塘 id                                              |
| fname        | 是   | string | 鱼塘名称                                             |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| number       | 是   | int    | 特等奖位次，只有为特等奖时有效                       |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| starttime    | 是   | int    | 奖品生效时间                                         |
| endtime      | 是   | int    | 奖品失效时间                                         |
| vid          | 是   | int    | 核销人 id                                            |
| successid    | 是   | int    | 领取人 id                                            |
| success      | 是   | int    | 是否领取， 1 领取 0 未领取                           |
| successtime  | 是   | int    | 领取时间                                             |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalpage: 3,
            totalcount: 10,
            list: [
               {
                  pid: 1,
                  pname: '平台红包',
                  fid: 1,
                  fname: '哈哈哈',
                  specialprize: 0,
                  number: 1,
                  prizetype: 1,
                  amount: 12,
                  others: '',
                  freestate: 1,
                  freeamount: 0,
                  starttime: 12121212121,
                  endtime: 1212121212,
                  vid: 1,
                  success: 1,
                  successid: 12121,
                  successtime: 232323232323,
               }
           ]
        }
    }
```
