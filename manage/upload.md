## 图片上传接口

**接口说明**

上传图片，获取 url

**URL**

/upload/img

**HTTP 请求方式**

post

**特殊 Headers**

```
contentType: 'multipart/form-data'
```

**请求体**

| 参数 | 类型   | 示例 | 说明     |
| ---- | ------ | ---- | -------- |
| file | object | .... | 图片文件 |

**响应 Headers**

无特殊响应 Header。

**响应体**

| 参数 | 类型   | 示例            | 说明     |
| ---- | ------ | --------------- | -------- |
| url  | string | https://img.png | 图片地址 |

**示例**

请求

URI: `http://127.0.0.1:3000/upload/img

响应

```
    {
        code:1,
        message: "success",
        data: {
            url: 'https://img.png'
        }
    }
```
