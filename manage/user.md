《绎安户外 pc 服务端用户接口定义》

# 概述

绎安户外 pc 管理服务端(manage)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/manage`或`https://{host}/manage`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/manage/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 用户登陆

**URL**

/user/login

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型   | 说明   |
| -------- | ---- | ------ | ------ |
| account  | 是   | string | 用户名 |
| password | 是   | string | 密码   |

**响应体**

| 参数  | 必填 | 类型   | 说明                                           |
| ----- | ---- | ------ | ---------------------------------------------- |
| state | 是   | int    | 1: `登陆成功`; 2: `账号不存在` 3: `密码错误`   |
| desc  | 是   | string | `success` / `uesr not find` / `password error` |
| role  | 是   | int    | 1 `管理员` 0 `普通用户`                        |

```
    {
        code: 1,  // code
        msg: 'ok',
        data: {
            state: 1,
            desc: 'success',
            role: 1
        }
    }
```

## 用户退出登陆

**URL**

/use/logout

**HTTP 请求方式**

GET

**请求体**
无

**响应体**
|参数|必填|类型|说明|
|---|---|---|---|
|state|是|int|1 `退出成功` 0 `退出失败`|
|desc|是|string|1 `success` 0 `fail`|

```
    {
        code: 1,
        msg: 'ok',
        data: {
            state: 1,
            desc: 'success'
        }
    }
```

## 用户密码修改

**URL**

/user/password

**HTTP 请求方式**

POST

**请求体**

| 参数   | 必填 | 类型   | 说明     |
| ------ | ---- | ------ | -------- |
| oldpwd | 是   | string | 原有密码 |
| newpwd | 是   | string | 新密码   |

**响应体**

| 参数  | 必填 | 类型 | 说明                          |
| ----- | ---- | ---- | ----------------------------- |
| state | 是   | int  | 1 `修改成功` 0 `原有密码错误` |
| desc  | 是   | int  | 1 `success` 0 `oldpwd error`  |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 用户列表获取

**URL**

/user/list

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型   | 说明              |
| -------- | ---- | ------ | ----------------- |
| uname    | 是   | string | 员工姓名          |
| state    | 是   | int    | 1 已启用 0 未启用 |
| pagenum  | 是   | int    | 页码,从 1 开始    |
| pagesize | 是   | int    | 页大小            |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 员工列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

list

| 参数     | 必填 | 类型   | 说明              |
| -------- | ---- | ------ | ----------------- |
| userid   | 是   | int    | 员工编号          |
| state    | 是   | int    | 1 已启用 0 未启用 |
| uname    | 是   | string | 员工姓名          |
| number   | 是   | int    | 员工工号          |
| branchid | 是   | int    | 部门 id           |
| branch   | 是   | string | 部门名称          |
| account  | 是   | string | 账号名称          |
| role     | 是   | int    | 1 管理员 0 业务员 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           totalpage: 1,
           totalcount: 10,
           list: [
             {
                userid: 1,
                uname: 'jahha',
                number: 121,
                state: 1,
                brancdid: 1,
                branch: '工程部',
                account: '121211dfd',
                role: 0
             }
           ]
        }
    }
```

## 核销人列表查询

**URL**

/user/verify

**HTTP 请求方式**

GET

**请求体**

| 参数   | 必填 | 类型   | 说明   |
| ------ | ---- | ------ | ------ |
| kwords | 是   | string | 关键字 |

**响应体**

| 参数 | 必填 | 类型          | 说明 |
| ---- | ---- | ------------- | ---- |
| list | 是   | array<object> | 列表 |

list

| 参数      | 必填 | 类型   | 说明                           |
| --------- | ---- | ------ | ------------------------------ |
| vid       | 是   | int    | 核销人 vid                     |
| vname     | 是   | string | 核销人姓名                     |
| openid    | 是   | string | openid                         |
| role      | 是   | int    | 1 鱼塘塘主 0 普通用户          |
| mobile    | 是   | string | 手机                           |
| subscribe | 是   | string | 是否关注公众号 1 关注 0 未关注 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           list: [
             {
                vid: 1,
                vname: 'jahha',
                openid: 121,
                role: 1,
                mobile: 1,
                subscribe: 1
             }
           ]
        }
    }
```

## 新增业务员

**URL**

/user/new

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型   | 说明              |
| -------- | ---- | ------ | ----------------- |
| state    | 是   | int    | 1 已启用 0 未启用 |
| uname    | 是   | string | 员工姓名          |
| number   | 是   | int    | 员工工号          |
| branchid | 是   | int    | 部门 id           |
| branch   | 是   | string | 部门名称          |
| account  | 是   | string | 账号名称          |
| role     | 是   | int    | 1 管理员 0 业务员 |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `新建成功` 0 `新建失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 编辑业务员

**URL**

/user/editor

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型   | 说明              |
| -------- | ---- | ------ | ----------------- |
| userid   | 是   | int    | 员工 userid       |
| state    | 是   | int    | 1 已启用 0 未启用 |
| uname    | 是   | string | 员工姓名          |
| number   | 是   | int    | 员工工号          |
| brancdid | 是   | int    | 部门 id           |
| branch   | 是   | string | 部门名称          |
| account  | 是   | string | 账号名称          |
| role     | 是   | int    | 1 管理员 0 业务员 |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `新建成功` 0 `新建失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 删除业务员

**URL**

/user/delete

**HTTP 请求方式**

POST

**请求体**

| 参数   | 必填 | 类型 | 说明        |
| ------ | ---- | ---- | ----------- |
| userid | 是   | int  | 员工 userid |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `删除成功` 0 `删除失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```
