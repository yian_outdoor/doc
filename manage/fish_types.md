《绎安户外 pc 服务端鱼塘类型接口定义》

# 概述

绎安户外 pc 管理服务端(manage)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/manage`或`https://{host}/manage`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/manage/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 鱼塘类型列表获取

**URL**

/fishtype/list

**HTTP 请求方式**

GET

**请求体**
无
**响应体**

| 参数 | 必填 | 类型          | 说明         |
| ---- | ---- | ------------- | ------------ |
| list | 是   | array<object> | 鱼塘类型列表 |

object 格式

| 参数  | 必填 | 类型   | 说明         |
| ----- | ---- | ------ | ------------ |
| tid   | 是   | int    | 鱼塘类型 cid |
| tname | 是   | string | 鱼塘类型名称 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           list: [
               {
                    tid: 1,
                    tname: '浙江杭州'
               },
               {
                    tid: 2,
                    tname: '浙江宁波'
               },
           ]
        }
    }
```

## 新建鱼塘类型

**URL**

/fishtype/new

**HTTP 请求方式**

POST

**请求体**

| 参数  | 必填 | 类型   | 说明         |
| ----- | ---- | ------ | ------------ |
| tname | 是   | string | 鱼塘类型名称 |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `新建成功` 0 `新建失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```

## 鱼塘类型删除

**URL**

/fishtype/delete

**HTTP 请求方式**

POST

**请求体**

| 参数 | 必填 | 类型 | 说明         |
| ---- | ---- | ---- | ------------ |
| tid  | 是   | int  | 鱼塘类型 cid |

**响应体**

| 参数  | 必填 | 类型 | 说明                      |
| ----- | ---- | ---- | ------------------------- |
| state | 是   | int  | 1 `删除成功` 0 `删除失败` |
| desc  | 是   | int  | 1 `success` 0 `fail`      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           state: 1,
           desc: 'success'
        }
    }
```
