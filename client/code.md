## 二维码扫描获得奖品信息

**URL**

/code/accept

**HTTP 请求方式**

POST

**请求体**

| 参数 | 必填 | 类型   | 说明           |
| ---- | ---- | ------ | -------------- |
| enid | 是   | string | 二维码 加密 id |

**响应体**

> 如果没有特等奖，则 specialprize 不存在， 如果奖品过期，specialprize 也不存在

| 参数         | 必填 | 类型         | 说明                                                                |
| ------------ | ---- | ------------ | ------------------------------------------------------------------- |
| expire       | 是   | int          | 是否已过期， 1 正常 2 二维码已过期 3 已领取                         |
| prize        | 是   | object       | 奖品                                                                |
| prizelist    | 是   | list<object> | 普通奖品列表                                                        |
| specailprize | 否   | list<object> | 特等奖,这个地方的特等奖，是一个数组，理论上有可能同时出现多个特等奖 |

object 格式

| 参数         | 必填 | 类型   | 说明                                                       |
| ------------ | ---- | ------ | ---------------------------------------------------------- |
| rid          | 是   | int    | 奖品记录 rid                                               |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                                   |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖                    |
| amount       | 是   | int    | 平台红包，单位是分                                         |
| others       | 是   | string | 其他优惠                                                   |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                                   |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分       |
| fid          | 是   | int    | 鱼塘 fid                                                   |
| fname        | 是   | string | 鱼塘名称                                                   |
| adstate      | 是   | int    | 是否有赞助商, 1 有 0 没有                                  |
| sname        | 是   | string | 赞助商名称， 只有 adstate = 1 时有效                       |
| logo         | 是   | string | 赞助商 logo 只有 adstate = 1 时有效                        |
| adcontent    | 是   | string | 赞助商广告语 只有 adstate = 1 时有效                       |
| gettime      | 是   | int    | 领取时间，该字段只有 expire 为 3 的时候有效                |
| endtime      | 是   | int    | 过期时间，该字段只有 expire 为 2 的时候有效                |
| isprize      | 否   | int    | 特等奖独有参数，是否可以领取这个特等奖， 1 可以， 0 不可以 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            prize: {
                rid: 1,  
                specialprize： 0,
                prizetype: 1,
                amount: 12121,
                others: '哈哈哈哈',
                freestate: 1,
                freeamount: 12121
            },
            prizelist: [
                {
                    rid: 1,  
                    specialprize： 0,
                    prizetype: 1,
                    amount: 12121,
                    others: '哈哈哈哈',
                    freestate: 1,
                    freeamount: 12121
                }
            ],
            specialprize: [
                {
                    rid: 2,
                    isPrize: 1,
                    specialprize：1,
                    prizetype: 0,
                    amount: 12121,
                    others: '哈哈哈哈',
                    freestate: 1,
                    freeamount: 12121
                }
            ]
        }
    }
```

## 扫描二维码之后，绑定该奖到用户

**URL**

/prize/bind

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型   | 说明                                                |
| ------------ | ---- | ------ | --------------------------------------------------- |
| rid          | 是   | int    | 奖品 rid                                            |
| specialprize | 是   | int    | 是否为特等奖                                        |
| payid        | 是   | string | 如果此奖需要付钱，须传此参数， 由支付接口返回此参数 |

**响应体**

| 参数 | 必填 | 类型   | 说明                        |
| ---- | ---- | ------ | --------------------------- |
| code | 是   | int    | 1 `获取成功` 其他`获取失败` |
| msg  | 是   | string | `ok` `fail`                 |

```
    {
        code: 1,
        msg: 'ok',
        data: {}
    }
```

## 实物奖品二维码兑换扫描

**URL**

/code/matter

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型 | 说明         |
| ------------ | ---- | ---- | ------------ |
| rid          | 是   | int  | 奖品记录 id  |
| specialprize | 是   | int  | 是否为特等奖 |

**响应体**

>

| 参数    | 必填 | 类型   | 说明                                  |
| ------- | ---- | ------ | ------------------------------------- |
| verify  | 是   | int    | 1 是本奖品核销人， 0 不是本奖品核销人 |
| prize   | 是   | object | verify 为零时。该字段不存在           |
| expire  | 是   | int    | 奖品是否过期 0 过期 1 没有过期        |
| success | 是   | int    | 是否已经核销 1 已核销 0 未核销        |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| id           | 是   | int    | 核销记录 id,此 id 用于核销接口                       |
| rid          | 是   | int    | 奖品记录 id                                          |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            prize: {
              id: 1,  
              rid: 1,  
              specialprize： 0,
              prizetype: 1,
              amount: 12121,
              others: '哈哈哈哈',
              freestate: 1,
              freeamount: 12121
              freeamount: 12121
            }
        }
    }
```
