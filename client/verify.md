## 待核销记录总数

**URL**

/verify/count

**HTTP 请求方式**

GET

**请求体**
无

**响应体**

| 参数  | 必填 | 类型 | 说明     |
| ----- | ---- | ---- | -------- |
| count | 是   | int  | 待核销数 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            count: 10
        }
    }
```

## 核销列表获取

**URL**

/verify/list

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型 | 说明                       |
| -------- | ---- | ---- | -------------------------- |
| success  | 是   | int  | 核销状态 1 已核销 0 未核销 |
| pagesize | 是   | int  | 页大小                     |
| pagenum  | 是   | int  | 页码                       |

**响应体**

| 参数       | 必填 | 类型         | 说明     |
| ---------- | ---- | ------------ | -------- |
| list       | 是   | list<object> | 奖品列表 |
| totalcount | 是   | int          | 总条数   |
| totalpage  | 是   | int          | 总页数   |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| id          | 是   | int    | 核销记录 id                                          |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| nickname     | 是   | string | 申请人的昵称                                         |
| userid       | 是   | int    | 申请人的用户 id   
| endtime      |是    | int    |  有效期结束时间|
| starttime    |是    | int     |有效期开始时间|                                   |
| time         | 是   | string | 申请时间                                             |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalpage: 1,
            totalcount: 10,
            list: [
                {
                    rid: 1,  
                    specialprize： 0,
                    prizetype: 1,
                    amount: 12121,
                    others: '哈哈哈哈',
                    freestate: 1,
                    freeamount: 12121,
                    nickname: '121212',
                    userid: 11111,
                    time: '2018/2/10 14:00'
                }
            ]
        }
    }
```

## 奖品核销

**URL**

/verify/prize

**HTTP 请求方式**

POST

**请求体**

>核销奖品如果需要付钱，则参数中须传payid,由支付接口返回

| 参数         | 必填 | 类型 | 说明        |
| ------------ | ---- | ---- | ----------- |
| id          | 是   | int  |  核销记录id |
| payid        | 是   | int  | 支付订单号        |

**响应体**

无

code 为 1 核销成功

```
    {
        code: 1,
        msg: 'ok',
        data: {}
    }
```
