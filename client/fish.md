《绎安户外 clinet 客户端鱼塘接口定义》

# 概述

绎安户外 客户端(client)提供的接口文档。

# 申明

此接口文档，为第一个版本，后续需要的接口和调用接口需传入的参数会陆续补充，请关注更新

# 接口描述

为保证安全性，所有消息交互均以 HTTPS 方式交互。

所有接口省略了 Base URL：`https://{ip}:{port}/client`或`https://{host}/client`。

业务 URL 格式：`Base URL`与接口中的`URL`相拼接为完整请求 URL。

`https://www.yianhuwai.club/client/${path}`

HTTP 消息通用头：

```
Accept:application/json;charset=utf-8;
Content-Type:application/json;charset=utf-8;
```

请求和响应消息的所有内容均为 UTF-8 编码。

HTTP 响应数据通用模板,所有响应数据均在 data 对象中

```
    {
        code: 1,
        msg: 'ok',
        data: {

        }
    }
```

# 接口详情

## 首页鱼塘列表获取

**URL**

/fish/list

**HTTP 请求方式**

POST

**请求体**

> 鱼塘列表查询接口，如果 cid、tid、name 均不传，则查找所有鱼塘,并按number排序

| 参数     | 必填 | 类型 | 说明           |
| -------- | ---- | ---- | -------------- |
| cid      | 否   | int  | 城市 id        |
| tid      | 否   | int  | 鱼塘类型 id    |
| pagenum  | 是   | int  | 页码,从 1 开始 |
| pagesize | 是   | int  | 页大小         |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 鱼塘列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数       | 必填 | 类型          | 说明           |
| ---------- | ---- | ------------- | -------------- |
| fid        | 是   | int           | 鱼塘 fid       |
| number     | 是   | int           | 该鱼塘序号     |
| fname      | 是   | string        | 鱼塘名称       |
| tid        | 是   | int           | 鱼塘类型 id    |
| tname      | 是   | string        | 鱼塘类型名称   |
| cid        | 是   | int           | 所属城市 id    |
| cname      | 是   | string        | 所属城市名称   |
| address    | 是   | string        | 详细地址       |
| contact    | 是   | string        | 联系人         |
| mobile     | 是   | string        | 联系人电话     |
| vid        | 是   | int           | 核销人 id      |
| vname      | 是   | string        | 核销人名称     |
| salesmanid | 是   | int           | 业务员 id      |
| salesman   | 是   | string        | 业务员名称     |
| sid        | 是   | int           | 赞助商 id      |
| sname      | 是   | string        | 赞助商名称     |
| saddress   | 是   | string        | 赞助商详细地址 |
| list       | 是   | array<object> | 出标率列表     |

list 中 object 格式

| 参数  | 必填 | 类型 | 说明        |
| ----- | ---- | ---- | ----------- |
| time  | 是   | int  | 时间      |
| total | 是   | int  | 标鱼总数    |
| count | 是   | int  | 出标数      |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalpage: 3,
            totalcount: 10,
            list: [
               {
                    fid: 1,
                    number: 1,
                    fname: '钱塘江',
                    tid: 1,
                    tname: '黑坑',
                    cid: 1,
                    cname: '浙江杭州',
                    address: '浙江杭州钱塘江大桥',
                    contact: '马云',
                    mobile: '15555555555',
                    vid: 1,
                    vname: '马化腾',
                    salesmanid: 111,
                    salesman: '李彦宏',
                    sid: 1,
                    sname: 'facebook'，
                    saddress: '钱塘江',
                    list: [
                      {
                        time: '2018/02/07',
                        total: 30,
                        count: 12
                      },
                      {
                        time: '2018/02/06',
                        total: 30,
                        count: 12
                      }
                      ....
                    ]
               }
           ]
        }
    }
```


## 城市列表获取

**URL**

/city/list

**HTTP 请求方式**

GET

**请求体**
无
**响应体**

| 参数 | 必填 | 类型          | 说明     |
| ---- | ---- | ------------- | -------- |
| list | 是   | array<object> | 城市列表 |

object 格式

| 参数 | 必填 | 类型   | 说明     |
| ---- | ---- | ------ | -------- |
| cid   | 是   | int    | 城市 cid  |
| cname | 是   | string | 城市名称 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           list: [
               {
                    cid: 1,
                    cname: '浙江杭州'
               },
               {
                    cid: 2,
                    cname: '浙江宁波'
               },
           ]
        }
    }
```

## 鱼塘类型列表获取

**URL**

/fishtype/list

**HTTP 请求方式**

GET

**请求体**
无
**响应体**

| 参数 | 必填 | 类型          | 说明     |
| ---- | ---- | ------------- | -------- |
| list | 是   | array<object> | 鱼塘类型列表 |

object 格式

| 参数 | 必填 | 类型   | 说明     |
| ---- | ---- | ------ | -------- |
| tid   | 是   | int    | 鱼塘类型 cid  |
| tname | 是   | string | 鱼塘类型名称 |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           list: [
               {
                    tid: 1,
                    tname: '浙江杭州'
               },
               {
                    tid: 2,
                    tname: '浙江宁波'
               },
           ]
        }
    }
```