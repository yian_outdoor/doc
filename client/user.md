
# 接口详情

## 用户登陆


**URL**

/user/login?code=`code`

**HTTP 请求方式**

GET

**请求体**

| 参数 | 必填 | 类型  | 说明      |
| ---- | ---- | ----- | --------- |
| code | 是   | sting | 微信 code |

**响应体**

| 参数      | 必填 | 类型   | 说明                                                          |
| --------- | ---- | ------ | ------------------------------------------------------------- |
| userid    | 是   | int    | 用户 6 位数 id                                                |
| openid    | 是   | string | 微信用户在公众号的 openid                                     |
| role      | 是   | int    | 用户角色 1 鱼塘塘主 0 普通用户                                |
| nickname  | 是   | string | 用户昵称                                                      |
| gender    | 是   | int    | 用户的性别, 值为 1 时是男性, 值为 2 时是女性, 值为 0 时是未知 |
| city      | 是   | string | 普通用户个人资料填写的城市                                    |
| province  | 是   | string | 用户个人资料填写的省份                                        |
| country   | 是   | string | 国家                                                          |
| avatarurl | 是   | string | 用户头像 URL，用户没有头像时该项为空。                        |
| mobile    | 是   | string | 用户手机号，第一次调用为空                                    |
| address   | 是   | string | 用户详细地址                                                  |
| subscribe | 是   | string | 是否关注公众号 1 关注 0 未关注                                |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           id: 100001,
           openid: 'dfdfdfdfdfdfdf',
           nickName: 'xiyangyang',
           gender: 1,
           mobile: '1121212121',
           address: '滨江区',
           language: 'zh_CN',
           city: '杭州',
           province: '浙江',
           country: '中国',
           avatarUrl: 'https://avatar.com/1.png',
           subscribe: 1
        }
    }
```


## 用户信息精确查找


**URL**

/user/info?userid=`userid`

**HTTP 请求方式**

GET

**请求体**

| 参数 | 必填 | 类型  | 说明      |
| ---- | ---- | ----- | --------- |
| userid | 是   | int | 用户id |

**响应体**

| 参数      | 必填 | 类型   | 说明                                                          |
| --------- | ---- | ------ | ------------------------------------------------------------- |
| userid    | 是   | int    | 用户 6 位数 id                                                |
| openid    | 是   | string | 微信用户在公众号的 openid                                     |
| role      | 是   | int    | 用户角色 1 鱼塘塘主 0 普通用户                                |
| nickname  | 是   | string | 用户昵称                                                      |
| gender    | 是   | int    | 用户的性别, 值为 1 时是男性, 值为 2 时是女性, 值为 0 时是未知 |
| city      | 是   | string | 普通用户个人资料填写的城市                                    |
| province  | 是   | string | 用户个人资料填写的省份                                        |
| country   | 是   | string | 国家                                                          |
| avatarurl | 是   | string | 用户头像 URL，用户没有头像时该项为空。                        |
| mobile    | 是   | string | 用户手机号，第一次调用为空                                    |
| address   | 是   | string | 用户详细地址                                                  |
| subscribe | 是   | string | 是否关注公众号 1 关注 0 未关注                                |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           id: 100001,
           openid: 'dfdfdfdfdfdfdf',
           nickName: 'xiyangyang',
           gender: 1,
           mobile: '1121212121',
           address: '滨江区',
           language: 'zh_CN',
           city: '杭州',
           province: '浙江',
           country: '中国',
           avatarUrl: 'https://avatar.com/1.png',
           subscribe: 1
        }
    }
```
