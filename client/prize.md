# 接口详情

## 个人中心奖品推荐

**URL**

/prize/recommend

**HTTP 请求方式**

GET

**请求体**

无

**响应体**

| 参数 | 必填 | 类型          | 说明     |
| ---- | ---- | ------------- | -------- |
| list | 是   | array<object> | 奖品列表 |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| rid          | 是   | int    | 优惠卷 id                                            |
| prizestate   | 是   | int    | 0 未使用 1 已使用 2 已过期 3 处理中                  |
| pid          | 是   | int    | 奖品 pid                                             |
| pname        | 是   | string | 奖品名称                                             |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| starttime    | 是   | int    | 有效期起始时间                                       |
| endtime      | 是   | int    | 有效期结束时间                                       |
| fid          | 是   | int    | 鱼塘 id                                              |
| fname        | 是   | string | 鱼塘名称                                             |
| adsponsor    | 是   | int    | 赞助商 id                                            |
| sname        | 是   | string | 赞助商名称                                           |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            list: [
               {
                  rid: 111,
                  pid: 1,
                  prizestate: 1,
                  pname: '钱塘江',
                  specialprize: 1,
                  prizetype: 1,
                  number: 1,
                  amount: 12,
                  others: '鱼钩',
                  freestate: 1,
                  freeamount: 0,
                  starttime: 112121212,
                  endtime: 121212121,
                  fid: 1,
                  fname: '阿里巴巴'，
                  adsponsor： 111，
                  sname: '腾讯'
               }
               ...
           ]
        }
    }
```

## 奖品列表

**URL**

/prize/list

**HTTP 请求方式**

POST

**请求体**

| 参数       | 必填 | 类型 | 说明                                |
| ---------- | ---- | ---- | ----------------------------------- |
| prizestate | 是   | int  | 0 未使用 1 已使用 2 已过期 3 处理中 |
| pagenum    | 是   | int  | 页码,从 1 开始                      |
| pagesize   | 是   | int  | 页大小                              |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 奖品列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------- |
| rid          | 是   | int    | 优惠卷 id                                            |
| prizestate   | 是   | int    | 0 未使用 1 已使用 2 已过期 3 处理中                  |
| pid          | 是   | int    | 奖品 pid                                             |
| pname        | 是   | string | 奖品名称                                             |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| starttime    | 是   | int    | 有效期起始时间                                       |
| endtime      | 是   | int    | 有效期结束时间                                       |
| fid          | 是   | int    | 鱼塘 id                                              |
| fname        | 是   | string | 鱼塘名称                                             |
| adsponsor    | 是   | int    | 赞助商 id                                            |
| sname        | 是   | string | 赞助商名称                                           |

```
    {
        code: 1,
        msg: 'ok',
        data: {
           totalpage: 1,
           totalcount: 10，
           list: [
             {
                rid: 111,
                pid: 1,
                prizestate: 1,
                pname: '钱塘江',
                specialprize: 1,
                prizetype: 1,
                number: 1,
                amount: 12,
                others: '鱼钩',
                freestate: 1,
                freeamount: 0,
                starttime: 112121212,
                endtime: 121212121,
                fid: 1,
                fname: '阿里巴巴'，
                adsponsor： 111，
                sname: '腾讯'
               }
           ]
        }
    }
```

## 奖品兑换

**URL**

/prize/change

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型 | 说明         |
| ------------ | ---- | ---- | ------------ |
| rid          | 是   | int  | 优惠卷 rid   |
| specialprize | 是   | int  | 是否为特等奖 |

**响应体**

| 参数 | 必填 | 类型   | 说明                        |
| ---- | ---- | ------ | --------------------------- |
| code | 是   | int    | 1 `兑换成功` 其他`兑换失败` |
| msg  | 是   | string | `ok` `fail`                 |

```
    {
        code: 1,
        msg: 'ok',
        data: {}
    }
```


## 奖品兑换码获取

**URL**

/prize/code

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型 | 说明         |
| ------------ | ---- | ---- | ------------ |
| rid          | 是   | int  | 奖品 rid     |
| specialprize | 是   | int  | 是否为特等奖 |

**响应体**

| 参数 | 必填 | 类型   | 说明                        |
| ---- | ---- | ------ | --------------------------- |
| code | 是   | int    | 1 `获取成功` 其他`获取失败` |
| msg  | 是   | string | `ok` `fail`                 |
| url  | 是   | string | 二维码地址                  |

```
    {
        code: 1,
        msg: 'ok',
        data: {
          url: ''
        }
    }
```

## 奖品转让

**URL**

/prize/transfer

**HTTP 请求方式**

POST

**请求体**

| 参数         | 必填 | 类型 | 说明         |
| ------------ | ---- | ---- | ------------ |
| rid          | 是   | int  | 奖品 rid     |
| specialprize | 是   | int  | 是否为特等奖 |
| userid       | 是   | int  | 转让人 id    |

**响应体**

| 参数 | 必填 | 类型   | 说明                        |
| ---- | ---- | ------ | --------------------------- |
| code | 是   | int    | 1 `转让成功` 其他`转让失败` |
| msg  | 是   | string | `ok` `fail`                 |

```
    {
        code: 1,
        msg: 'ok',
        data: {}
    }
```

## 领奖记录

**URL**

/prize/records

**HTTP 请求方式**

POST

**请求体**

| 参数     | 必填 | 类型 | 说明           |
| -------- | ---- | ---- | -------------- |
| pagenum  | 是   | int  | 页码,从 1 开始 |
| pagesize | 是   | int  | 页大小         |

**响应体**

| 参数       | 必填 | 类型          | 说明                                 |
| ---------- | ---- | ------------- | ------------------------------------ |
| list       | 是   | array<object> | 奖品列表                             |
| totalpage  | 是   | int           | 该查找条件下 and 该页大小 下共有几页 |
| totalcount | 是   | int           | 该查找条件下共有多少条数据           |

object 格式

| 参数         | 必填 | 类型   | 说明                                                 |
| ------------ | ---- | ------ | ---------------------------------------------------                                              |
| specialprize | 是   | int    | 是否为特等奖 1 是 0 不是                             |
| prizetype    | 是   | int    | 奖品类型 0 平台红包 1 塘主红包 2 实物奖              |
| amount       | 是   | int    | 平台红包，单位是分                                   |
| others       | 是   | string | 其他优惠                                             |
| freestate    | 是   | int    | 是否免费 1 免费 0 不免费                             |
| freeamount   | 是   | int    | 此字段只有在 freestate = 0 时有效， 收钱金额，单位分 |
| fname        | 是   | string | 鱼塘名称                                             |
| gettime      | 是   | int    | 领奖时间                                             |

```
    {
        code: 1,
        msg: 'ok',
        data: {
            totalcount: 10,
            totalpage: 20,
            list: [
                {
                    rid: 1,
                    specialprize: 1,
                    prizetype: 0,
                    amount: 10,
                    freestate: 0,
                    freeamount: 1,
                    fid: 1,
                    fname: '淡淡的',
                    gettime: 1112121
                }
            ]
        }
    }
```
